package transcoder

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"sync"

	"github.com/pkg/errors"
)

var lock = &sync.Mutex{}

func TranscodeMP3ToDCA(mp3 []byte, out io.Writer) error {
	lock.Lock()
	var err error
	defer lock.Unlock()
	var buf = &bytes.Buffer{}
	os.Remove("sound.mp3")
	ioutil.WriteFile("sound.mp3", mp3, os.ModePerm)
	ffmpeg := exec.Command("ffmpeg",
		"-i", "sound.mp3",
		"-af", "volume=0.5", // Hack for lowering the volume of the sound file
		"-f", "s16le",
		"-ar", "48000",
		"-ac", "2",
		"pipe:1")
	ffmpeg.Stdout = buf
	err = ffmpeg.Run()
	if err != nil {
		return errors.Wrap(err, "Could not Run ffmpeg")
	}
	dca := exec.Command("dca")
	dca.Stdin = buf
	dca.Stdout = out
	err = dca.Run()
	if err != nil {
		return errors.Wrap(err, "Could not run DCA")
	}
	return nil
}
