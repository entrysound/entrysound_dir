module entrysound_dir

require (
	cloud.google.com/go v0.37.4
	firebase.google.com/go v3.7.0+incompatible
	github.com/bwmarrin/discordgo v0.19.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/wire v0.2.1
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/mux v1.7.1
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.4.1
	golang.org/x/crypto v0.0.0-20190422183909-d864b10871cd // indirect
	golang.org/x/net v0.0.0-20190420063019-afa5a82059c6 // indirect
	golang.org/x/oauth2 v0.0.0-20190402181905-9f3314589c9a // indirect
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894 // indirect
	google.golang.org/api v0.3.2
)
